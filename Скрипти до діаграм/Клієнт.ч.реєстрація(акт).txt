@startuml
start
  :Виведення повідомлення\n "Введіть дані про себе";
  :Ввід даних користувачем;
  note right: ім'я, прізвище, по-батькові,\n дата народження, місце проживання,\n номер телефону
  repeat:Виведення повідомлення\n "Введіть логін і пароль";
  :Ввід логіну користувачем;
  note left: email
  :Ввід паролю користувачем;
  :Перевірка на стороні сервера\n чи користувач з даним логіном\n уже зареєстрований;
  :Очікування відповіді від сервера;
  :Аналіз отриманих даних;
  backward:Виведення повідомлення\n "Такий користувач\n уже зареєстрований";
  repeat while (Користувач з даним логіном\n уже зареєстрований) is (Так)
  ->Ні; 
  :Вхід на головну сторіку сайту;
stop
@enduml